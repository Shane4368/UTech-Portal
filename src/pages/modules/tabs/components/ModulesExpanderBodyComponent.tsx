import React from "react";
import { FaTrash } from "react-icons/fa";
import { v4 as uuidv4 } from "uuid";

import { SelectableModulesTable } from "../../components/selectablemodules/SelectableModulesTable";

type ModuleSearchFilterProps = {
    id: string;
    setModuleFilters: React.Dispatch<React.SetStateAction<JSX.Element[]>>;
};

function ModuleSearchFilter(props: ModuleSearchFilterProps): JSX.Element {
    function handleDeleteFilterButtonClick(e: React.MouseEvent): void {
        props.setModuleFilters(filters => filters.filter(x => x.key !== props.id));
    }

    return (
        <div className="module-search-filter display-flex">
            <select className="flex-grow-10">
                <option>-- Select option --</option>
                <option>Level</option>
                <option>Credits</option>
                <option>Department</option>
                <option>Tutor</option>
            </select>
            <input className="flex-grow-10" type="text" />
            <button
                className="filter-delete-button flex-grow-1"
                title="Delete search filter"
                onClick={handleDeleteFilterButtonClick}>
                <FaTrash />
            </button>
        </div>
    );
}




type ModulesExpanderBodyComponentProps = {
    timeOfDay: "morning" | "afternoon" | "evening";
};

export function ModulesExpanderBodyComponent(props: ModulesExpanderBodyComponentProps): JSX.Element {
    const [moduleFilters, setModuleFilters] = React.useState<JSX.Element[]>([]);

    React.useEffect(() => setModuleFilters([]), [props.timeOfDay]);

    function handleAddFilterButtonClick(e: React.MouseEvent): void {
        setModuleFilters(x => {
            const id = uuidv4();

            return [...x, (
                <ModuleSearchFilter
                    key={id}
                    id={id}
                    setModuleFilters={setModuleFilters}
                />
            )];
        });
    }

    return (
        <React.Fragment>
            <div className="module-search-container">
                <div className="display-flex">
                    <input className="flex-grow-10" type="search" placeholder="Module Code/Name" />
                    <input className="flex-grow-1" type="button" value="Search" />
                    <input
                        className="flex-grow-1"
                        type="button"
                        value="Add filter"
                        onClick={handleAddFilterButtonClick}
                    />
                </div>
                {moduleFilters}
            </div>

            <div className="overflow-x-auto">
                <SelectableModulesTable timeOfDay={props.timeOfDay} />
            </div>
        </React.Fragment>
    );
}