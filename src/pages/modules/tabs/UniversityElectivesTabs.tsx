import React from "react";

import { ExpanderComponent } from "../../../components/expander/ExpanderComponent";
import { TabComponent } from "../../../components/tabs/TabsComponent";
import { ModulesExpanderBodyComponent } from "./components/ModulesExpanderBodyComponent";

export function UniversityElectivesTabs(): JSX.Element {
    return (
        <TabComponent identifier="tab-afternoon-university-electives" label="University Electives">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 4 credits in Summer Session from University Electives - Main Campus</p>
                    <p>University Elective</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="afternoon" />
            </ExpanderComponent>
        </TabComponent>
    );
}