import React from "react";

import { ExpanderComponent } from "../../../components/expander/ExpanderComponent";
import { TabComponent } from "../../../components/tabs/TabsComponent";
import { ModulesExpanderBodyComponent } from "./components/ModulesExpanderBodyComponent";

export function SpecialisedMorningModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-morning-specialised-modules" label="Morning">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 21 credits in Summer Session from Specialised Modules - BSCOMP</p>
                    <p>BSCOMP Specialised Modules (Morning)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="morning" />
            </ExpanderComponent>
        </TabComponent>
    );
}




export function SpecialisedAfternoonModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-afternoon-specialised-modules" label="Afternoon">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 21 credits in Summer Session from Specialised Modules - BSCOMP</p>
                    <p>BSCOMP Specialised Modules (Afternoon)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="afternoon" />
            </ExpanderComponent>
        </TabComponent>
    );
}




export function SpecialisedEveningModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-evening-specialised-modules" label="Evening">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 21 credits in Summer Session from Specialised Modules - BSCOMP</p>
                    <p>BSCOMP Specialised Modules (Evening)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="evening" />
            </ExpanderComponent>
        </TabComponent>
    );
}