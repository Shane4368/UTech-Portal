import React from "react";

import { ExpanderComponent } from "../../../components/expander/ExpanderComponent";
import { TabComponent } from "../../../components/tabs/TabsComponent";
import { ModulesExpanderBodyComponent } from "./components/ModulesExpanderBodyComponent";

export function ElectiveMorningModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-morning-elective-modules" label="Morning">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 8 credits in Summer Session from Elective Modules - BSCOMP</p>
                    <p>BSCOMP Elective Modules (Morning)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="morning" />
            </ExpanderComponent>
        </TabComponent>
    );
}




export function ElectiveAfternoonModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-afternoon-elective-modules" label="Afternoon">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 8 credits in Summer Session from Elective Modules - BSCOMP</p>
                    <p>BSCOMP Elective Modules (Afternoon)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="afternoon" />
            </ExpanderComponent>
        </TabComponent>
    );
}




export function ElectiveEveningModulesTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-evening-elective-modules" label="Evening">
            <ExpanderComponent>
                <React.Fragment>
                    <p>Choose a maximum of 8 credits in Summer Session from Elective Modules - BSCOMP</p>
                    <p>BSCOMP Elective Modules (Evening)</p>
                </React.Fragment>
                <ModulesExpanderBodyComponent timeOfDay="evening" />
            </ExpanderComponent>
        </TabComponent>
    );
}