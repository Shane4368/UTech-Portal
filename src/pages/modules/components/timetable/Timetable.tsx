import React from "react";
import { v4 as uuidv4 } from "uuid";

import modulesData from "./timetable.json";

import "./Timetable.scss";

type ModuleData = {
    code: string;
    name: string;
    type: string;
    tutor: string;
    startDate: Date;
    duration: number;
};

type TrashTableCell = {
    startRowIndex: number;
    columnIndex: number;
    total: number;
};

type TableCell = {
    type: "th" | "td";
    column?: string;
    content?: string;
    hoverText?: string;
    rowSpan?: number;
};

function getDayOfWeek(dayIndex: number): string {
    return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"][dayIndex];
}

function getDayOfWeekIndex(date: Date): number {
    const day = date.getDay() - 1;
    return day >= 0 ? day : 6;
}




function tableBodyRecords(): JSX.Element[] {
    const modules = modulesData.map(x => {
        (x.startDate as unknown as Date) = new Date(x.startDate);
        return x;
    }) as unknown as ModuleData[];

    const trashCells: TrashTableCell[] = [];
    const records: TableCell[][] = [];

    // loop i: 8 AM to 9 PM
    for (let i = 8; i <= 21; i++) {
        const filteredModules = modules.filter(x => x.startDate.getHours() === i);

        if (filteredModules.length === 0) {
            records.push([
                { type: "th", content: `${i}:00` },
                { type: "td", column: getDayOfWeek(0) },
                { type: "td", column: getDayOfWeek(1) },
                { type: "td", column: getDayOfWeek(2) },
                { type: "td", column: getDayOfWeek(3) },
                { type: "td", column: getDayOfWeek(4) },
                { type: "td", column: getDayOfWeek(5) },
                { type: "td", column: getDayOfWeek(6) }
            ]);

            continue;
        }

        const cells: TableCell[] = [];

        // loop j: Monday to Sunday
        for (let j = 0; j < 7; j++) {
            const module = filteredModules.find(x => getDayOfWeekIndex(x.startDate) === j);

            if (module === undefined) {
                cells.push({ type: "td", column: getDayOfWeek(j) });
                continue;
            }

            const duration = module.duration > 1 ? `${module.duration} hours` : "1 hour";
            const title = `${module.code} ${module.name}\nType: ${module.type}\nDuration: ${duration}`;

            cells.push({
                type: "td",
                rowSpan: module.duration,
                hoverText: title,
                column: getDayOfWeek(j),
                content: `${module.code} ${module.name}\n` +
                    `Type: ${module.type}\n` +
                    `Staff: ${module.tutor}\n` +
                    "Room: Online"
            });

            if (module.duration > 1) {
                trashCells.push({
                    startRowIndex: i - 8,
                    columnIndex: j + 1,
                    total: module.duration - 1
                });
            }
        }

        cells.unshift({ type: "th", content: `${i}:00` });
        records.push(cells);
    }

    // delete unnecessary cells
    for (const cell of trashCells) {
        for (let i = cell.startRowIndex + cell.total; i > cell.startRowIndex; i--) {
            records[i].splice(cell.columnIndex, 1);
        }
    }

    return records.map(cells => (
        <tr key={uuidv4()}>
            {
                cells.map(cell => {
                    if (cell.type === "th")
                        return (<th key={uuidv4()}>{cell.content}</th>);

                    return (
                        <td
                            key={uuidv4()}
                            rowSpan={cell.rowSpan}
                            title={cell.hoverText}
                            data-column={cell.column}>
                            {cell.content}
                        </td>
                    );
                })
            }
        </tr>
    ));
}




function TimetableComponent(props: { ref: React.RefObject<HTMLTableElement>; }, ref: React.ForwardedRef<HTMLTableElement>): JSX.Element {
    return (
        <table className="timetable" ref={ref}>
            <caption>
                Timetable for Academic Year 2020/21<br />
                <span>8/24/20-12/31/20</span>
            </caption>
            <thead>
                <tr>
                    <th></th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                </tr>
            </thead>

            <tbody>
                {tableBodyRecords()}
            </tbody>
        </table>
    );
}

export const Timetable = React.forwardRef(TimetableComponent);