import React from "react";
import { v4 as uuidv4 } from "uuid";

import modulesData from "./modules.json";

import "./SelectableModulesTable.scss";

type SelectableModulesTableProps = {
    timeOfDay: "morning" | "afternoon" | "evening";
};

export function SelectableModulesTable(props: SelectableModulesTableProps): JSX.Element {
    const [modules, setModules] = React.useState<JSX.Element[]>();

    React.useEffect(() => {
        const timeOfDay = props.timeOfDay === "morning" ? "M"
            : props.timeOfDay === "afternoon" ? "N" : "E";

        setModules(modulesData.flatMap(x => {
            const arr = [];

            for (let i = 1; i <= 3; i++) {
                arr.push(
                    <tr key={uuidv4()}>
                        <td>{x.code}</td>
                        <td>{x.name}</td>
                        <td>{`SEM${x.period}`}</td>
                        <td>{`U${timeOfDay}${i}`}</td>
                        <td>{x.level}</td>
                        <td>{x.credit}</td>
                    </tr>
                );
            }

            return arr;
        }));
    }, [props.timeOfDay]);

    return (
        <table className="selectable-modules-table">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Period</th>
                    <th>Occ</th>
                    <th>Level</th>
                    <th>Credit</th>
                </tr>
            </thead>
            <tbody>
                {modules}
            </tbody>
        </table>
    );
}