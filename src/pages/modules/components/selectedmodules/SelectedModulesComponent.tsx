import { FaTrash } from "react-icons/fa";
import { v4 as uuidv4 } from "uuid";

import modulesData from "../selectablemodules/modules.json";

import "./SelectedModulesComponent.scss";

export function SelectedModulesComponent(): JSX.Element {
    return (
        <div className="selected-modules">
            {
                modulesData.map(x => (
                    <div key={uuidv4()} className="selected-module">
                        <div className="selected-module-name">{`${x.code} ${x.name}`}</div>
                        <div>{`SEM${x.period} • UM1 • Level ${x.level} • ${x.credit} Credits`}</div>
                        <button><FaTrash /></button>
                    </div>
                ))
            }
        </div>
    );
}