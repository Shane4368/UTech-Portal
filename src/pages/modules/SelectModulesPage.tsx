import React from "react";
import { FaTimes } from "react-icons/fa";

import { FooterComponent } from "../../components/footer/FooterComponent";
import { NavigationBarComponent } from "../../components/navigationbar/NavigationBarComponent";
import { SelectedModulesComponent } from "./components/selectedmodules/SelectedModulesComponent";
import { TabsComponent } from "../../components/tabs/TabsComponent";
import { Timetable } from "./components/timetable/Timetable";
import { useOpenClose } from "../../components/modal/useOpenClose";

import {
    ModalComponent,
    ModalDialogBodyComponent,
    ModalDialogHeaderComponent
} from "../../components/modal/ModalComponent";

import {
    SpecialisedAfternoonModulesTab,
    SpecialisedEveningModulesTab,
    SpecialisedMorningModulesTab
} from "./tabs/SpecialisedModulesTabs";

import {
    ElectiveAfternoonModulesTab,
    ElectiveEveningModulesTab,
    ElectiveMorningModulesTab
} from "./tabs/ElectiveModulesTabs";

import { UniversityElectivesTabs } from "./tabs/UniversityElectivesTabs";

import "./SelectModulesPage.scss";

export function SelectModulesPage(): JSX.Element {
    const dialogCloseButtonRef = React.useRef<HTMLButtonElement>(null);
    const timetableRef = React.useRef<HTMLTableElement>(null);
    const downloadTimetableSelectRef = React.useRef<HTMLSelectElement>(null);

    const [isDialogOpen, setDialogOpen] = useOpenClose(dialogCloseButtonRef);

    function handleViewTimetableButtonClick(e: React.MouseEvent): void {
        setDialogOpen(true);
    }

    function handleDownloadTimetableButtonClick(e: React.MouseEvent): void {
        const select = downloadTimetableSelectRef.current!;

        if (select.value === "pdf") {
            const newWindow = window.open();

            if (newWindow === null) return;

            const meta = document.querySelector<HTMLMetaElement>("meta[name='viewport']");

            const styles = document.querySelectorAll<HTMLStyleElement | HTMLLinkElement>(
                process.env.NODE_ENV === "development"
                    ? "head style" : "link[rel='stylesheet']"
            );

            newWindow.document.write("<head>");
            newWindow.document.write(meta!.outerHTML);
            newWindow.document.write(Array.prototype.map.call(styles, x => x.outerHTML).join(""));
            newWindow.document.write("</head>");

            newWindow.document.title = "Timetable | UTechJa E:Vision Portal";
            newWindow.document.documentElement.dataset.theme = document.documentElement.dataset.theme;
            newWindow.document.write(timetableRef.current!.outerHTML);
            newWindow.document.close();
            newWindow.print();
        }
    }

    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Select Modules" />

            <main className="viewport main select-modules-page display-flex">
                <div>
                    <section>
                        <p>
                            Choose a minimum of 1 and a maximum of 21 credits from the modules contained within this block.
                        </p>
                    </section>

                    <section>
                        <h2>Specialised Modules</h2>
                        <p>Choose modules from each tab.</p>
                        <TabsComponent>
                            <SpecialisedMorningModulesTab />
                            <SpecialisedAfternoonModulesTab />
                            <SpecialisedEveningModulesTab />
                        </TabsComponent>
                    </section>

                    <section>
                        <h2>Elective Modules</h2>
                        <p>Choose modules from one of any tab.</p>
                        <TabsComponent>
                            <ElectiveMorningModulesTab />
                            <ElectiveAfternoonModulesTab />
                            <ElectiveEveningModulesTab />
                        </TabsComponent>
                    </section>

                    <section>
                        <h2>University Electives</h2>
                        <p>Choose a university elective.</p>
                        <TabsComponent>
                            <UniversityElectivesTabs />
                        </TabsComponent>
                    </section>
                </div>

                <div>
                    <h2>Selected Modules</h2>
                    <p>You have selected 6 modules.</p>
                    <SelectedModulesComponent />
                    <input
                        type="button"
                        value="View timetable"
                        onClick={handleViewTimetableButtonClick}
                    />
                    <input type="button" value="Submit selections" />
                </div>
            </main>

            <FooterComponent />

            <ModalComponent className="viewport" open={isDialogOpen}>
                <ModalDialogHeaderComponent>
                    <button ref={dialogCloseButtonRef} title="Close dialog">
                        <FaTimes />
                    </button>
                    <h2>View Timetable Dialog</h2>
                </ModalDialogHeaderComponent>
                <ModalDialogBodyComponent>
                    <div className="overflow-x-auto">
                        <Timetable ref={timetableRef} />
                    </div>

                    <div className="display-flex">
                        <select ref={downloadTimetableSelectRef}>
                            <option value="ics">iCalendar (.ics)</option>
                            <option value="pdf">Portable Document Format (.pdf)</option>
                        </select>
                        <input
                            type="button"
                            value="Download"
                            title="Download timetable"
                            onClick={handleDownloadTimetableButtonClick}
                        />
                    </div>
                </ModalDialogBodyComponent>
            </ModalComponent>

        </React.Fragment>
    );
}