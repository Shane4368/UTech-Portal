import React from "react";

type CheckboxComponentProps = {
    inputId: string;
    label: string;
    checked?: boolean;
};

export function CheckboxComponent(props: CheckboxComponentProps): JSX.Element {
    return (
        <React.Fragment>
            <input id={props.inputId} type="checkbox" defaultChecked={props.checked ?? false} />
            <label htmlFor={props.inputId}>{props.label}</label>
        </React.Fragment>
    );
}