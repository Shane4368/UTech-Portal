import React from "react";

import { ThemeContext } from "../../../../contexts/ThemeContext";

import "./ThemeButtonComponent.scss";

type ThemeButtonComponentProps = {
    value: string;
    label: string;
};

export function ThemeButtonComponent(props: ThemeButtonComponentProps): JSX.Element {
    const [isActive, setActive] = React.useState(false);

    const themeContext = React.useContext(ThemeContext)!;

    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(() => setActive(props.value === themeContext.theme), [themeContext]);

    function handleClick(e: React.MouseEvent): void {
        themeContext.setTheme(props.value);
    }

    return (
        <button
            className="theme-button"
            data-value={props.value}
            data-active={isActive}
            onClick={handleClick}>

            <div className="theme-button-box"></div>
            <div className="theme-button-label">{props.label}</div>
        </button>
    );
}