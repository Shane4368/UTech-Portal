import { TabComponent } from "../../../components/tabs/TabsComponent";
import { CheckboxComponent } from "../components/CheckboxComponent";
import { ThemeButtonComponent } from "../components/themebutton/ThemeButtonComponent";

export function PersonalisationTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-personalisation" label="Personalisation">
            <div className="section section-themes">
                <h2>Themes</h2>
                <div className="display-flex">
                    <ThemeButtonComponent value="light" label="Light" />
                    <ThemeButtonComponent value="dark" label="Dark" />
                    <ThemeButtonComponent value="system" label="System" />
                </div>
            </div>

            <div className="section section-content-boxes">
                <h2>Content Boxes</h2>
                <h3>Home</h3>
                <ul>
                    <li>
                        <CheckboxComponent
                            inputId="show-important-dates"
                            label="Show Important Dates"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-student-enrolment-status"
                            label="Show Student Enrolment Status"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-notice-board"
                            label="Show Notice Board"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-intray"
                            label="Show Intray"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-personal-links"
                            label="Show Personal Links"
                            checked
                        />
                    </li>
                </ul>

                <h3>Student</h3>
                <ul>
                    <li>
                        <CheckboxComponent
                            inputId="show-students-reports"
                            label="Show Student's Reports"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-student-actions"
                            label="Show Student Action"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-useful-links"
                            label="Show Useful Links"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-academic-information"
                            label="Show Academic Information"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-fee-information"
                            label="Show Fee Information"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-career-n-placements"
                            label="Show Career and Placements"
                            checked
                        />
                    </li>
                    <li>
                        <CheckboxComponent
                            inputId="show-academic-advisor"
                            label="Show Academic Advisor"
                            checked
                        />
                    </li>
                </ul>
            </div>
        </TabComponent>
    );
}