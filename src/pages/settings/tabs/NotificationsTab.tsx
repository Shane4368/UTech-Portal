import { TabComponent } from "../../../components/tabs/TabsComponent";
import { CheckboxComponent } from "../components/CheckboxComponent";

export function NotificationsTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-notifications" label="Notifications">
            <div className="section section-notifications">
                <h2>Notifications</h2>
                <p>
                    <CheckboxComponent
                        inputId="cb-notice-board"
                        label="Email when items are added to Notice Board"
                        checked
                    />
                </p>
                <p>
                    <CheckboxComponent
                        inputId="cb-intray"
                        label="Email when items are added to Intray"
                        checked
                    />
                </p>
                <p>
                    <CheckboxComponent
                        inputId="cb-student-enrolment-status"
                        label="Email when Student Enrolment Status is updated"
                        checked
                    />
                </p>
                <p>
                    <CheckboxComponent
                        inputId="cb-student-actions"
                        label="Email when Student Actions is updated"
                        checked
                    />
                </p>
                <p>
                    <CheckboxComponent
                        inputId="cb-academic-advisor"
                        label="Email when Academic Advisor is updated"
                        checked
                    />
                </p>
            </div>
        </TabComponent>
    );
}