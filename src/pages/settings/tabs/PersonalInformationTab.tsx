import { TabComponent } from "../../../components/tabs/TabsComponent";

export function PersonalInformationTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-personal-information" label="Personal Information">
            <div className="section section-contact-information">
                <h2>Contact Information</h2>
                <div className="display-flex">
                    <img
                        className="avatar"
                        alt="avatar"
                        src="https://cdn.discordapp.com/avatars/432692479083544596/ba4c762978116eea63c92735b37ec61f.png?size=128"
                    />

                    <address>
                        237 Old Hope Road<br />
                        Kingston 6<br />
                        Jamaica W.I.<br />
                        (876) 927-1680
                    </address>
                </div>
            </div>

            <div className="section section-personal-information">
                <h2>Personal Information</h2>
                <p>
                    This screen allows you to modify the personal details that are stored on your record.
                    If a field has an asterisk (*) beside it then that field is mandatory and must contain
                    a value. Fields that are highlighted in grey are non-editable and their values cannot
                    be changed online. If there are any changes to your name, marital status or nationality
                    you must contact the Records Office. All other fields are both optional and editable.
                    Please provide as much information as possible.
                </p>
                <div className="overflow-x-auto">
                    <table>
                        <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Field Value</th>
                                <th>Translation</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Student Code</th>
                                <td><input type="number" value="0000000" disabled /></td>
                                <td>Someone Suspicious</td>
                            </tr>
                            <tr>
                                <th scope="row">Title*</th>
                                <td>
                                    <select defaultValue="Mr">
                                        <option>-- Select title --</option>
                                        <option>Captain</option>
                                        <option>Dr</option>
                                        <option>Major</option>
                                        <option>Miss</option>
                                        <option>Mr</option>
                                        <option>Mrs</option>
                                        <option>Ms</option>
                                        <option>Pastor</option>
                                        <option>Professor</option>
                                        <option>Reverend</option>
                                        <option>Sir</option>
                                        <option>Sister</option>
                                    </select>
                                </td>
                                <td>Mr</td>
                            </tr>
                            <tr>
                                <th scope="row">Surname</th>
                                <td><input value="SUSPICIOUS" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Former Name</th>
                                <td><input disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Forename 1</th>
                                <td><input value="SOMEONE" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Forname 2</th>
                                <td><input disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Forename 3</th>
                                <td><input disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Forename used*</th>
                                <td><input value="SOMEONE" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Full/Official Name</th>
                                <td><input value="Someone Suspicious" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Gender</th>
                                <td><input value="M" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Date of Birth*</th>
                                <td><input value="31/Dec/2020" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">TRN (without dashes)</th>
                                <td><input type="number" value="123456789" disabled /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Marital Status</th>
                                <td><input value="S" disabled /></td>
                                <td>SINGLE</td>
                            </tr>
                            <tr>
                                <th scope="row">Nationality</th>
                                <td><input type="number" value="680" disabled /></td>
                                <td>JAMAICAN</td>
                            </tr>
                            <tr>
                                <th scope="row">Country of Birth*</th>
                                <td><input type="number" value="680" disabled /></td>
                                <td>JAMAICA</td>
                            </tr>
                            <tr>
                                <th scope="row">Country of Residence</th>
                                <td><input type="number" value="680" disabled /></td>
                                <td>JAMAICA</td>
                            </tr>
                            <tr>
                                <th scope="row">Disability</th>
                                <td>
                                    <select defaultValue="NO DISABILITY">
                                        <option>-- Select disability --</option>
                                        <option>BLIND/PARTIALLY SIGHTED</option>
                                        <option>DEAF/HEARING IMPAIRMENT</option>
                                        <option>DYSLEXIA</option>
                                        <option>INFORMATION NOT SOUGHT</option>
                                        <option>MENTAL HEALTH DIFFICULTY</option>
                                        <option>MULTIPLE DISABILITY</option>
                                        <option>NO DISABILITY</option>
                                        <option>NOT KNOWN</option>
                                        <option>OTHER DISABILITY</option>
                                        <option>PERSONAL CARE SUPPORT</option>
                                        <option>UNSEEN DISABILITY EG DIABETES, EPILEPSY</option>
                                        <option>WHEELCHAIR USER/MOBILITY DIFFICULTY</option>
                                    </select>
                                </td>
                                <td>NO DISABILITY</td>
                            </tr>
                            <tr>
                                <th scope="row">Highest Education Level</th>
                                <td>
                                    <select defaultValue="CAPE 2">
                                        <option>-- Select education level --</option>
                                        <option>Accreditation of Prior (Experimental) Learning</option>
                                        <option>Associate Degree</option>
                                        <option>Bachelor's Degree</option>
                                        <option>CAPE 1</option>
                                        <option>CAPE 2</option>
                                        <option>CXC</option>
                                        <option>Certificate - Postgraduate</option>
                                        <option>Certificate - Undergraduate</option>
                                        <option>GCE 'A'</option>
                                        <option>GCE 'AS' Levels</option>
                                        <option>GCE 'O'</option>
                                        <option>Grade 11 of High School</option>
                                        <option>Grade 12 of High School</option>
                                        <option>Grade 9 of High School</option>
                                        <option>Master's Degree</option>
                                        <option>Mature stu admitted on prev exp/ins entrance exam</option>
                                        <option>Mixed CAPE and GCE 'A'</option>
                                        <option>Mixed CAPE and GCE 'O'</option>
                                        <option>No Formal Qualification</option>
                                        <option>Not Known</option>
                                        <option>Overseas High School</option>
                                        <option>PhD Degree</option>
                                        <option>Professional Qualifications</option>
                                    </select>
                                </td>
                                <td>CAPE 2</td>
                            </tr>
                            <tr>
                                <th scope="row">Who will pay your fees?</th>
                                <td>
                                    <select defaultValue="PARENTS">
                                        <option>-- Select option --</option>
                                        <option>JAMAICAN GOVT</option>
                                        <option>OTHER</option>
                                        <option>OTHER FAMILY</option>
                                        <option>OVERSEAS GOVT</option>
                                        <option>PARENTS</option>
                                        <option>PRIVATE</option>
                                        <option>SELF</option>
                                        <option>SLB</option>
                                    </select>
                                </td>
                                <td>PARENTS</td>
                            </tr>
                            <tr>
                                <th scope="row">Religion</th>
                                <td>
                                    <select defaultValue="CHRISTIANITY">
                                        <option>-- Select religion --</option>
                                        <option>BUDDHISM</option>
                                        <option>CHRISTIANITY</option>
                                        <option>HINDUISM</option>
                                        <option>JEW</option>
                                        <option>MUSLIM</option>
                                        <option>RASTAFARIANISM</option>
                                    </select>
                                </td>
                                <td>CHRISTIANITY</td>
                            </tr>
                            <tr>
                                <th scope="row">Denomination</th>
                                <td>
                                    <select defaultValue="-- Select denomination --">
                                        <option>-- Select denomination --</option>
                                        <option>ADVENTIST</option>
                                        <option>ANGLICAN</option>
                                        <option>BAPTIST</option>
                                        <option>BRETHREN</option>
                                        <option>CATHOLIC</option>
                                        <option>CHURCH OF CHRIST</option>
                                        <option>JEHOVAH WITNESS</option>
                                        <option>METHODIST</option>
                                        <option>MISSIONARY</option>
                                        <option>MORVIAN</option>
                                        <option>MORMON</option>
                                        <option>NON-DENOMINATION</option>
                                        <option>OTHER</option>
                                        <option>PENTECOSTAL</option>
                                        <option>PRESBYTERIAN</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <th scope="row">Institution Email</th>
                                <td><input value="SOMEONESUSPICIOUS@students.utech.edu.jm" disabled /></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <input type="button" value="Save personal information" />
            </div>
        </TabComponent>
    );
}