import { TabComponent } from "../../../components/tabs/TabsComponent";

export function SecurityTab(): JSX.Element {
    return (
        <TabComponent identifier="tab-security" label="Security">
            <div className="section section-change-password">
                <h2>Change Password</h2>
                <div className="overflow-x-auto">
                    <table>
                        <tbody>
                            <tr>
                                <th scope="row">Enter current password</th>
                                <td><input /></td>
                            </tr>
                            <tr>
                                <th scope="row">Enter new password</th>
                                <td><input /></td>
                            </tr>
                            <tr>
                                <th scope="row">Confirm new password</th>
                                <td><input /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div className="section section-security-questions-and-answers">
                <h2>Security Questions and Answers</h2>
                <p>Enter password to change security questions and password.</p>
                <div className="overflow-x-auto">
                    <table>
                        <tbody>
                            <tr>
                                <th scope="row">Enter password</th>
                                <td><input /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </TabComponent>
    );
}