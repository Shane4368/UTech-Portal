import React from "react";

import { NavigationBarComponent } from "../../components/navigationbar/NavigationBarComponent";
import { FooterComponent } from "../../components/footer/FooterComponent";
import { TabsComponent } from "../../components/tabs/TabsComponent";

import { PersonalisationTab } from "./tabs/PersonalisationTab";
import { PersonalInformationTab } from "./tabs/PersonalInformationTab";
import { NotificationsTab } from "./tabs/NotificationsTab";
import { SecurityTab } from "./tabs/SecurityTab";

import "./SettingsPage.scss";

export function SettingsPage(): JSX.Element {
    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Settings" />

            <main className="viewport">
                <TabsComponent headerPosition="left">
                    <PersonalInformationTab />
                    <PersonalisationTab />
                    <NotificationsTab />
                    <SecurityTab />
                </TabsComponent>
            </main>

            <FooterComponent />
        </React.Fragment>
    );
}