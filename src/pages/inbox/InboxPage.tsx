import React from "react";
import { Link, Outlet } from "react-router-dom";

import { FooterComponent } from "../../components/footer/FooterComponent";
import { NavigationBarComponent } from "../../components/navigationbar/NavigationBarComponent";
import { InboxComponent } from "./components/inbox/InboxComponent";
import { InboxMessageComponent } from "./components/inboxmessage/InboxMessageComponent";

import "./InboxPage.scss";

export function InboxPageOutlet(): JSX.Element {
    return (<Outlet />);
}




export function InboxPage(): JSX.Element {
    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Inbox" />

            <main className="viewport overflow-x-auto">
                <InboxComponent />
            </main>

            <FooterComponent />
        </React.Fragment>
    );
}




export function InboxMessagePage(): JSX.Element {
    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Inbox Message" />

            <main className="viewport main inbox-message-page">
                <Link to="/inbox" className="hyperlink-button">Back to inbox</Link>
                <div className="overflow-x-auto">
                    <InboxMessageComponent />
                </div>
            </main>

            <FooterComponent />
        </React.Fragment>
    );
}