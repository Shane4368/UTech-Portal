import { Link } from "react-router-dom";

import "./InboxComponent.scss";

export function InboxComponent(): JSX.Element {
    return (
        <table className="inbox">
            <caption>Your Inbox (1-4 of 4 messages)</caption>
            <thead>
                <tr>
                    <th>From</th>
                    <th>Subject</th>
                    <th>Received</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>UTech, Jamaica</td>
                    <td>Financial Clearance Status</td>
                    <td>04 Nov 2020</td>
                </tr>
                <tr className="position-relative">
                    <td><Link to="message">UTech, Jamaica</Link></td>
                    <td>Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</td>
                    <td>27 Aug 2020</td>
                </tr>
                <tr>
                    <td>UTech, Jamaica</td>
                    <td>Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</td>
                    <td>27 Aug 2020</td>
                </tr>
                <tr>
                    <td>UTech, Jamaica</td>
                    <td>Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</td>
                    <td>26 Aug 2020</td>
                </tr>
            </tbody>
        </table>
    );
}