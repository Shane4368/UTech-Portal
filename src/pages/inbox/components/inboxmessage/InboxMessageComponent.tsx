import "./InboxMessageComponent.scss";

export function InboxMessageComponent(): JSX.Element {
    return (
        <table className="inbox-message">
            <tbody>
                <tr>
                    <th>From</th>
                    <td>UTech, Jamaica</td>
                </tr>
                <tr>
                    <th>Received</th>
                    <td>27 Aug 2020</td>
                </tr>
                <tr>
                    <th>Due Date</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Subject</th>
                    <td>Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</td>
                </tr>
                <tr>
                    <th>Attachments</th>
                    <td><span className="fake-hyperlink">ODL-GuidelinesforStudents (751KB)</span></td>
                </tr>
                <tr>
                    <th>Message</th>
                    <td>
                        Dear Student<br />
                        WELCOME TO YOUR ONLINE MODULE.<br />
                        You have selected a module occurrence that is offered via online delivery or has a component that is delivered online: INT1001 Information Technology, Occurrence: UN3 !!!<br />
                        Click the attachment for more details.<br />
                        ISAS Administrator
                    </td>
                </tr>
            </tbody>
        </table>
    );
}