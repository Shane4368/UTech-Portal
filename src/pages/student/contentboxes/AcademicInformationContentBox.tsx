import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import academicInformationData from "../academic-information.json";

export function AcademicInformationContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={academicInformationData.label}
            colour="green">
            {
                academicInformationData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}