import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import studentActionsData from "../student-actions.json";

export function StudentActionsContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={studentActionsData.label}
            colour="blue">
            <tr>
                <td className="content-box-description">{studentActionsData.description}</td>
            </tr>
            {
                studentActionsData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}