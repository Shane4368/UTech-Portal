import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import careerAndPlacementsData from "../career-and-placements.json";

export function CareerPlacementsContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={careerAndPlacementsData.label}
            colour="green">
            {
                careerAndPlacementsData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}