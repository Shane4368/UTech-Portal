import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import "./StudentsReportsContentBox.scss";

export function StudentsReportsContentBox(): JSX.Element {
    return (
        <ContentBoxComponent className="content-box-students-reports" caption="Student's Reports" colour="orange">
            <tr>
                <td className="content-box-description">
                    Please select a report from the drop down list and click the 'View report' button
                    in order to view the report.
                </td>
            </tr>
            <tr>
                <td className="display-flex">
                    <select defaultValue="-- Select report --">
                        <option>-- Select report --</option>
                        <option>Provisional Transcript of Results</option>
                        <option>Exam Timetable</option>
                        <option>Account Balance Information</option>
                        <option>Account Transactions</option>
                    </select>
                    <input type="button" value="View report" />
                </td>
            </tr>
        </ContentBoxComponent>
    );
}