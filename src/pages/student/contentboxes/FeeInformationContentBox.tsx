import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import feeInformationData from "../fee-information.json";

export function FeeInformationContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={feeInformationData.label}
            colour="green">
            {
                feeInformationData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}