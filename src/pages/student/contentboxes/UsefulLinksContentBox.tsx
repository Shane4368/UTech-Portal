import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import usefulLinksData from "../useful-links.json";

export function UsefulLinksContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={usefulLinksData.label}
            colour="gold">
            <tr>
                <td className="content-box-description">{usefulLinksData.description}</td>
            </tr>
            {
                usefulLinksData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}