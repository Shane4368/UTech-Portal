import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

export function AcademicAdvisorContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption="Academic Advisor"
            colour="green">
            <tr>
                <td className="content-box-description">Information about your personal tutor.</td>
            </tr>
            <tr>
                <td>Details of your advisor</td>
            </tr>
        </ContentBoxComponent>
    );
}