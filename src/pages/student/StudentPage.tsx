import React from "react";

import { NavigationBarComponent } from "../../components/navigationbar/NavigationBarComponent";
import { FooterComponent } from "../../components/footer/FooterComponent";

import { AcademicInformationContentBox } from "./contentboxes/AcademicInformationContentBox";
import { FeeInformationContentBox } from "./contentboxes/FeeInformationContentBox";
import { CareerPlacementsContentBox } from "./contentboxes/CareerPlacementsContentBox";
import { AcademicAdvisorContentBox } from "./contentboxes/AcademicAdvisorContentBox";
import { StudentsReportsContentBox } from "./contentboxes/StudentsReportsContentBox";
import { StudentActionsContentBox } from "./contentboxes/StudentActionsContentBox";
import { UsefulLinksContentBox } from "./contentboxes/UsefulLinksContentBox";

import "../home/HomePage.scss"; // Because StudentPage has same layout.

export function StudentPage(): JSX.Element {
    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Student" />

            <main className="viewport main display-flex">
                <div className="display-flex">
                    <AcademicInformationContentBox />
                    <FeeInformationContentBox />
                    <CareerPlacementsContentBox />
                    <AcademicAdvisorContentBox />
                </div>

                <div className="display-flex position-sticky">
                    <StudentsReportsContentBox />
                    <StudentActionsContentBox />
                    <UsefulLinksContentBox />
                </div>
            </main>

            <FooterComponent />
        </React.Fragment>
    );
}