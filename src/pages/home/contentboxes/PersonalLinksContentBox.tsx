import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";
import { ExternalLinkComponent } from "../../../components/ExternalLinkComponent";

import personalLinksData from "../personal-links.json";

export function PersonalLinksContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={personalLinksData.label}
            colour="green">
            <tr>
                <td className="content-box-description">
                    {personalLinksData.description}
                </td>
            </tr>
            {
                personalLinksData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td>
                            <ExternalLinkComponent key={uuidv4()} to={x.url} label={x.name} />
                        </td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}