import { v4 as uuidv4 } from "uuid";

import importantDatesData from "../important-dates.json";

import "./ImportantDatesContentBox.scss";

export function ImportantDatesContentBox(): JSX.Element {
    return (
        <table className="content-box-important-dates content-box green">
            <caption>{importantDatesData.label}</caption>
            <thead>
                <tr>
                    <th colSpan={2}>{importantDatesData.description}</th>
                </tr>
            </thead>
            <tbody>
                {
                    importantDatesData.rows.map(x => (
                        <tr key={uuidv4()}>
                            <td>{x.date}</td>
                            <td>{x.event}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    );
}