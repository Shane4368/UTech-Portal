import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import intrayData from "../intray.json";

export function IntrayContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={intrayData.label}
            colour="blue">
            {
                intrayData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}