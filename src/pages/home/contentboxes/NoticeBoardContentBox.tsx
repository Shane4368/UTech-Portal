import { v4 as uuidv4 } from "uuid";

import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

import noticeBoardData from "../notice-board.json";

export function NoticeBoardContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption={noticeBoardData.label}
            colour="green">
            {
                noticeBoardData.rows.map(x => (
                    <tr key={uuidv4()}>
                        <td><span className="fake-hyperlink">{x}</span></td>
                    </tr>
                ))
            }
        </ContentBoxComponent>
    );
}