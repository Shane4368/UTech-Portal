import { ContentBoxComponent } from "../../../components/contentbox/ContentBoxComponent";

export function StudentEnrolmentStatusContentBox(): JSX.Element {
    return (
        <ContentBoxComponent
            caption="Student Enrolment Status"
            colour="blue">
            <tr>
                <td>Semester 1: Currently enrolled online</td>
            </tr>
        </ContentBoxComponent>
    );
}