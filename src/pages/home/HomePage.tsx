import React from "react";

import { NavigationBarComponent } from "../../components/navigationbar/NavigationBarComponent";
import { FooterComponent } from "../../components/footer/FooterComponent";

import { NoticeBoardContentBox } from "./contentboxes/NoticeBoardContentBox";
import { IntrayContentBox } from "./contentboxes/IntrayContentBox";
import { PersonalLinksContentBox } from "./contentboxes/PersonalLinksContentBox";
import { ImportantDatesContentBox } from "./contentboxes/ImportantDatesContentBox";
import { StudentEnrolmentStatusContentBox } from "./contentboxes/StudentEnrolmentStatusContentBox";

import "./HomePage.scss";

export function HomePage(): JSX.Element {
    return (
        <React.Fragment>
            <NavigationBarComponent pageTitle="Home" />

            <main className="viewport main display-flex">
                <div className="display-flex">
                    <NoticeBoardContentBox />
                    <IntrayContentBox />
                    <PersonalLinksContentBox />
                </div>

                <div className="display-flex position-sticky">
                    <ImportantDatesContentBox />
                    <StudentEnrolmentStatusContentBox />
                </div>
            </main>

            <FooterComponent />
        </React.Fragment>
    );
}