import React from "react";
import { useNavigate } from "react-router-dom";

import { UTechEmblemAndWebsiteNameComponent } from "./components/UTechEmblemAndWebsiteNameComponent";

import "./LoginPage.scss"; // Because SecurityQuestionsPage has same layout.

export function SecurityQuestionsPage(): JSX.Element {
    const navigate = useNavigate();

    React.useEffect(() => {
        document.title = "Security Questions | UTechJa E:Vision Portal";
    }, []);

    function handleSubmit(e: React.FormEvent): void {
        e.preventDefault();
        navigate("/home");
    }

    return (
        <main className="viewport main security-questions-page display-flex">
            <UTechEmblemAndWebsiteNameComponent
                orientation="vertical"
                horizontalOrientationAtBreakpoint={(_, windowWidth) => windowWidth <= 1155}
            />

            <form className="form form-security-questions" autoComplete="off" onSubmit={handleSubmit}>

                <div>
                    <h1>Security Questions</h1>
                    <p>Answer the questions displayed and then click on the "Continue" button.</p>
                </div>

                <div>
                    <label htmlFor="dob">What is your Date of Birth?</label>
                    <input id="dob" type="number" placeholder="DDMMYY" required />
                </div>

                <div>
                    <input className="form-button" type="submit" value="Continue" />
                </div>

            </form>
        </main>
    );
}