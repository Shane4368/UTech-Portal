import React from "react";

import { UTechEmblemComponent } from "../../../components/utechemblem/UTechEmblemComponent";

import "./UTechEmblemAndWebsiteNameComponent.scss";

type UTechEmblemAndWebsiteNameComponentProps = {
    orientation?: "horizontal" | "vertical";
    horizontalOrientationAtBreakpoint?: (width: number, windowWidth: number) => boolean;
    verticalOrientationAtBreakpoint?: (width: number, windowWidth: number) => boolean;
    static?: boolean;
};

export function UTechEmblemAndWebsiteNameComponent(props: UTechEmblemAndWebsiteNameComponentProps): JSX.Element {
    const selfRef = React.useRef<HTMLDivElement>(null);

    const horizontalAtBreakpointRef = React.useRef(
        props.horizontalOrientationAtBreakpoint ?? ((width: number) => width <= 584)
    );

    const verticalAtBreakpointRef = React.useRef(
        props.verticalOrientationAtBreakpoint ?? ((width: number) => width <= 390)
    );

    const [orientation, setOrientation] = React.useState(props.orientation);

    React.useEffect(() => {
        if (props.static) return;

        const self = selfRef.current!;

        const resizeObserver = new ResizeObserver(() => {
            const width = self.getBoundingClientRect().width;
            const windowWidth = window.innerWidth;

            if (props.orientation === "horizontal" || props.orientation == null) {
                setOrientation(
                    verticalAtBreakpointRef.current(width, windowWidth)
                        ? "vertical" : "horizontal"
                );
            }
            else {
                setOrientation(
                    horizontalAtBreakpointRef.current(width, windowWidth)
                        ? "horizontal" : "vertical"
                );
            }
        });

        resizeObserver.observe(self);
        return (): void => resizeObserver.unobserve(self);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div
            ref={selfRef}
            className="utech-emblem-and-website-name"
            data-orientation={orientation}>

            <UTechEmblemComponent />
            <div className="website-name">
                University of Technology, Jamaica E:Vision Portal
            </div>
        </div>
    );
}