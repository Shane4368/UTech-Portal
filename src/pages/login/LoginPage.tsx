import React from "react";
import { Outlet, useNavigate } from "react-router-dom";

import { UTechEmblemAndWebsiteNameComponent } from "./components/UTechEmblemAndWebsiteNameComponent";

import "./LoginPage.scss";

export function LoginPageOutlet(): JSX.Element {
    return (<Outlet />);
}

export { SecurityQuestionsPage } from "./SecurityQuestionsPage";

export function LoginPage(): JSX.Element {
    const navigate = useNavigate();

    React.useEffect(() => {
        document.title = "Login | UTechJa E:Vision Portal";
    }, []);

    function handleSubmit(e: React.FormEvent): void {
        e.preventDefault();
        navigate("./security-questions");
    }

    return (
        <main className="viewport main login-page display-flex">
            <UTechEmblemAndWebsiteNameComponent
                orientation="vertical"
                horizontalOrientationAtBreakpoint={(_, windowWidth) => windowWidth <= 1155}
            />

            <form className="form form-login" autoComplete="off" onSubmit={handleSubmit}>

                <div>
                    <h1>Login</h1>
                    <p className="colour-text-danger">
                        <u><strong>For UTech students only:</strong></u> Enter your ID number for your username.
                        If you are logging on for the first time, please use your date of birth as your password.
                        The new date format is ddmmyy without the slashes. Eg: 130773
                    </p>
                </div>

                <div>
                    <label htmlFor="username">Username</label>
                    <input id="username" type="text" required />
                </div>

                <div>
                    <label htmlFor="password">Password</label>
                    <input id="password" type="password" required />
                </div>

                <div>
                    <input className="form-button" type="submit" value="Login" />
                </div>

                <div>
                    <p>
                        If you have forgotten your password, please enter your username above
                        then click the link below to have a new password emailed to you.
                    </p>
                    <span className="fake-hyperlink">Forgot password?</span>
                </div>

            </form>
        </main>
    );
}