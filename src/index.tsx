import React from "react";
import ReactDOMClient from "react-dom/client";
import { Route, Routes, Navigate, BrowserRouter, HashRouter } from "react-router-dom";

import { LoginPageOutlet, LoginPage, SecurityQuestionsPage } from "./pages/login/LoginPage";
import { HomePage } from "./pages/home/HomePage";
import { StudentPage } from "./pages/student/StudentPage";
import { SettingsPage } from "./pages/settings/SettingsPage";
import { SelectModulesPage } from "./pages/modules/SelectModulesPage";
import { InboxPageOutlet, InboxPage, InboxMessagePage } from "./pages/inbox/InboxPage";

import { ThemeContext } from "./contexts/ThemeContext";

import "./index.scss";

function AppRoutes(): JSX.Element {
    return (
        <Routes>
            <Route path="/" element={<Navigate replace to="/login" />} />

            <Route path="/login/*" element={<LoginPageOutlet />}>
                <Route path="*" element={<LoginPage />} />
                <Route path="security-questions" element={<SecurityQuestionsPage />} />
            </Route>

            <Route path="/home" element={<HomePage />} />
            <Route path="/student" element={<StudentPage />} />
            <Route path="/settings" element={<SettingsPage />} />
            <Route path="/select-modules" element={<SelectModulesPage />} />

            <Route path="/inbox/*" element={<InboxPageOutlet />}>
                <Route path="*" element={<InboxPage />} />
                <Route path="message" element={<InboxMessagePage />} />
            </Route>
        </Routes>
    );
}

function App(): JSX.Element {
    const [theme, setTheme] = React.useState(localStorage.getItem("theme") ?? "system");

    React.useLayoutEffect(() => {
        document.documentElement.dataset.theme = theme;
        localStorage.setItem("theme", theme);
    }, [theme]);

    React.useEffect(() => {
        // used to set CSS property: image-rendering
        const brands = navigator.userAgentData?.brands;
        const browserBrand = brands?.[brands.length - 1].brand;

        if (browserBrand != null)
            document.documentElement.dataset.browserBrand = browserBrand;
    }, []);

    function getRouter() {
        return process.env.NODE_ENV === "development" ? BrowserRouter : HashRouter;
    }

    const Router = getRouter();

    return (
        <React.StrictMode>
            <Router>
                <ThemeContext.Provider value={{ theme, setTheme }}>
                    <AppRoutes />
                </ThemeContext.Provider>
            </Router>
        </React.StrictMode>
    );
}

ReactDOMClient
    .createRoot(document.getElementById("root")!)
    .render(<App />);