import React from "react";

export type ThemeContextValue = {
    theme: string;
    setTheme: React.Dispatch<React.SetStateAction<string>>;
} | null;

export const ThemeContext = React.createContext<ThemeContextValue>(null);