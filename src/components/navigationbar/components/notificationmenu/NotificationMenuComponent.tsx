import React from "react";
import { FaBell } from "react-icons/fa";
import { Link } from "react-router-dom";

import { MenuComponent } from "../../../menu/MenuComponent";

import "./NotificationMenuComponent.scss";

export function NotificationMenuComponent(): JSX.Element {
    return (
        <MenuComponent className="notification-menu">
            <FaBell />

            <React.Fragment>
                <div>
                    <Link to="/select-modules">
                        <span className="name">Student Actions</span>
                        <span className="content">Select your modules</span>
                    </Link>
                </div>
                <div>
                    <span className="name">Intray</span>
                    <span className="content">Financial Clearance Status</span>
                </div>
                <div>
                    <span className="name">Intray</span>
                    <span className="content">Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</span>
                </div>
                <div>
                    <span className="name">Intray</span>
                    <span className="content">Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</span>
                </div>
                <div>
                    <span className="name">Intray</span>
                    <span className="content">Online Blended Learning: Preliminary Guidelines - 2020/1, SEM1</span>
                </div>
            </React.Fragment>
        </MenuComponent>
    );
}