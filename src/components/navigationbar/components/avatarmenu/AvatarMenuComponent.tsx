import React from "react";
import { Link } from "react-router-dom";

import { MenuComponent } from "../../../menu/MenuComponent";

import "./AvatarMenuComponent.scss";

export function AvatarMenuComponent(): JSX.Element {
    return (
        <MenuComponent className="avatar-menu">
            <img
                className="avatar"
                alt="avatar"
                src="https://cdn.discordapp.com/avatars/432692479083544596/ba4c762978116eea63c92735b37ec61f.png?size=64"
            />

            <React.Fragment>
                <Link to="/inbox">Inbox</Link>
                <Link to="/settings">Settings</Link>
                <Link to="/">Logout</Link>
            </React.Fragment>
        </MenuComponent>
    );
}