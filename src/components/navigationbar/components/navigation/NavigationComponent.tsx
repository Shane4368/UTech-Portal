import { NavLink } from "react-router-dom";

import "./NavigationComponent.scss";

export function NavigationComponent(): JSX.Element {
    return (
        <nav className="navigation">
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/student">Student</NavLink>
        </nav>
    );
}