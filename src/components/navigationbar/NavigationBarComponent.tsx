import React from "react";

import { UTechEmblemComponent } from "../utechemblem/UTechEmblemComponent";
import { AvatarMenuComponent } from "./components/avatarmenu/AvatarMenuComponent";
import { NavigationComponent } from "./components/navigation/NavigationComponent";
import { NotificationMenuComponent } from "./components/notificationmenu/NotificationMenuComponent";

import "./NavigationBarComponent.scss";

type NavigationBarComponentProps = {
    pageTitle: string;
};

export function NavigationBarComponent(props: NavigationBarComponentProps): JSX.Element {
    React.useEffect(() => {
        document.title = props.pageTitle + " | UTechJa E:Vision Portal";
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <header className="navigation-bar">
            <div className="navigation-bar-top">
                <div className="viewport display-flex">
                    <UTechEmblemComponent />
                    <div className="flex-grow">
                        <p>Welcome to the UTechJa E:Vision Portal, Someone Suspicious.</p>
                        <div className="display-flex">
                            <NavigationComponent />
                            <NotificationMenuComponent />
                            <AvatarMenuComponent />
                        </div>
                    </div>
                </div>
            </div>

            <div className="navigation-bar-bottom viewport">
                <h1>{props.pageTitle}</h1>
            </div>
        </header>
    );
}