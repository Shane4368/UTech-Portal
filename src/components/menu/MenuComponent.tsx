import React from "react";

import "./MenuComponent.scss";

type MenuComponentProps = {
    className?: string;
    children: React.ReactNode[];
};

export function MenuComponent(props: MenuComponentProps): JSX.Element {
    const [menuFlyoutDisplay, setMenuFlyoutDisplay] = React.useState("none");

    function handleButtonClick(e: React.MouseEvent): void {
        setMenuFlyoutDisplay(menuFlyoutDisplay === "none" ? "block" : "none");
    }

    return (
        <div className={"menu " + props.className}>
            <button className="menu-button" onClick={handleButtonClick}>
                {props.children[0]}
            </button>
            <div className="menu-flyout" style={{ display: menuFlyoutDisplay }}>
                {props.children[1]}
            </div>
        </div>
    );
}