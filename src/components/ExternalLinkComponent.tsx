type ExternalLinkComponentProps = {
    to: string;
    label: string;
};

export function ExternalLinkComponent(props: ExternalLinkComponentProps): JSX.Element {
    return (
        <a
            target="_blank"
            rel="noopener noreferrer nofollow"
            href={props.to}>
            {props.label}
        </a>
    );
}