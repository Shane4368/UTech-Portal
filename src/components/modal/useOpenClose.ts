import React from "react";

export function useOpenClose(buttonRef: React.RefObject<HTMLButtonElement>): [boolean, React.Dispatch<React.SetStateAction<boolean>>] {
    const [isOpen, setOpen] = React.useState(false);

    React.useEffect(() => {
        const button = buttonRef.current!;
        const handleButtonClick = () => setOpen(false);

        button.addEventListener("click", handleButtonClick);
        return (): void => button.removeEventListener("click", handleButtonClick);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return [isOpen, setOpen];
}