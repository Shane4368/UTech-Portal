type ModalComponentProps = {
    open?: boolean;
    className: string;
    children: React.ReactNode;
};

export function ModalComponent(props: ModalComponentProps): JSX.Element {
    return (
        <div className="modal" style={{ display: props.open ? "block" : "none" }}>
            <div className={"modal-dialog " + props.className}>
                {props.children}
            </div>
        </div>
    );
}




type ModalDialogHeaderComponentProps = {
    children: React.ReactNode;
};

export function ModalDialogHeaderComponent(props: ModalDialogHeaderComponentProps): JSX.Element {
    return (
        <div className="modal-dialog-header">
            {props.children}
        </div>
    );
}




type ModalDialogBodyComponentProps = {
    children: React.ReactNode;
};

export function ModalDialogBodyComponent(props: ModalDialogBodyComponentProps): JSX.Element {
    return (
        <div className="modal-dialog-body">
            {props.children}
        </div>
    );
}




type ModalDialogFooterComponentProps = {
    children: React.ReactNode;
};

export function ModalDialogFooterComponent(props: ModalDialogFooterComponentProps): JSX.Element {
    return (
        <div className="modal-dialog-footer">
            {props.children}
        </div>
    );
}