import { ExternalLinkComponent } from "../ExternalLinkComponent";
import { UTechEmblemComponent } from "../utechemblem/UTechEmblemComponent";
import { ScrollToTopButtonComponent } from "./components/scrolltotopbutton/ScrollToTopButtonComponent";

import "./FooterComponent.scss";

const institutionName = "University of Technology, Jamaica";

export function FooterComponent(): JSX.Element {
    return (
        <footer className="footer">
            <ScrollToTopButtonComponent />

            <div className="footer-top">
                <div className="viewport display-flex">
                    <div className="footer-section footer-section-contact display-inline-flex">
                        <UTechEmblemComponent />
                        <address>
                            <span className="footer-section-title">{institutionName}</span>
                            <p>
                                237 Old Hope Road, Kingston 6, Jamaica W.I.<br />
                                Tel: (876) 927-1680-8<br />
                                Fax: (876) 977-4388 | (876) 927-1925
                            </p>
                        </address>
                    </div>

                    <div className="footer-section footer-section-social-media">
                        <span className="footer-section-title">Social Media</span>
                        <ul>
                            <li>
                                <ExternalLinkComponent
                                    to="https://www.facebook.com/UTechJa/"
                                    label="Facebook"
                                />
                            </li>
                            <li>
                                <ExternalLinkComponent
                                    to="https://www.instagram.com/utechjamaica/"
                                    label="Instagram"
                                />
                            </li>
                            <li>
                                <ExternalLinkComponent
                                    to="https://twitter.com/utechjamaica"
                                    label="Twitter"
                                />
                            </li>
                            <li>
                                <ExternalLinkComponent
                                    to="https://www.youtube.com/user/utechjamaica"
                                    label="YouTube"
                                />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="footer-bottom">
                <p className="viewport">
                    Copyright © 2021-2022 {institutionName}. All rights reserved.
                </p>
            </div>
        </footer>
    );
}