import React from "react";

import "./ScrollToTopButtonComponent.scss";

export function ScrollToTopButtonComponent(): JSX.Element {
    const [bottom, setBottom] = React.useState(-3);

    React.useEffect(() => {
        let lastScrollY = window.scrollY;

        const handleScroll = (): void => {
            setBottom(
                window.scrollY < lastScrollY && window.scrollY > 500
                    ? 1 : -3
            );

            lastScrollY = window.scrollY;
        };

        window.addEventListener("scroll", handleScroll);
        return (): void => window.removeEventListener("scroll", handleScroll);
    }, []);

    return (
        <input
            type="button"
            className="scroll-to-top-button"
            title="Scroll to top of page"
            value="Scroll to top"
            style={{ bottom: bottom + "em" }}
            onClick={(): void => window.scrollTo(0, 0)}
        />
    );
}