import React from "react";

export type TabsContextValue = {
    selectedIndex: number;
    setSelectedIndex: React.Dispatch<React.SetStateAction<number>>;

    setSelectedTab: React.Dispatch<React.SetStateAction<string | undefined>>;

    content: React.ReactNode;
    setContent: React.Dispatch<React.SetStateAction<React.ReactNode>>;
} | null;

export const TabsContext = React.createContext<TabsContextValue>(null);