import React from "react";

import { TabsContext } from "./TabsContext";

import "./TabsComponent.scss";

type TabComponentProps = {
    label: string;
    identifier?: string;
    children: React.ReactNode;
};

export function TabComponent(props: TabComponentProps): JSX.Element {
    const selfRef = React.useRef<HTMLButtonElement>(null);
    const indexRef = React.useRef(0);

    const [isSelected, setSelected] = React.useState(false);

    const tabsContext = React.useContext(TabsContext)!;

    React.useEffect(() => {
        const self = selfRef.current!;
        const index = Array.prototype.indexOf.call(self.parentElement!.children, self);
        indexRef.current = index;
    }, []);

    React.useEffect(() => {
        if (tabsContext.selectedIndex === indexRef.current) {
            tabsContext.setContent(props.children);
            tabsContext.setSelectedTab(props.identifier);
            setSelected(true);
        }
        else {
            setSelected(false);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tabsContext]);

    function handleButtonClick(e: React.MouseEvent): void {
        tabsContext.setSelectedIndex(indexRef.current);
        tabsContext.setSelectedTab(props.identifier);
    }

    return (
        <button
            ref={selfRef}
            className="tab"
            data-selected={isSelected}
            onClick={handleButtonClick}>
            {props.label}
        </button>
    );
}




type TabsComponentProps = {
    wrapBreakpoint?: number;
    headerPosition?: "left" | "right" | "top" | "bottom";
    children: React.ReactNode;
};

export function TabsComponent(props: TabsComponentProps): JSX.Element {
    const selfRef = React.useRef<HTMLDivElement>(null);

    const [headerPosition, setHeaderPosition] = React.useState(props.headerPosition ?? "top");
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const [selectedTab, setSelectedTab] = React.useState<string>();
    const [content, setContent] = React.useState<React.ReactNode>(null);

    const wrapBreakpoint = props.wrapBreakpoint ?? 715;
    const contextValue = {
        selectedIndex,
        setSelectedIndex,
        setSelectedTab,
        content,
        setContent
    };

    React.useEffect(() => {
        if (!["left", "right"].includes(headerPosition)) return;

        const self = selfRef.current!;

        const resizeObserver = new ResizeObserver(() => {
            const width = self.getBoundingClientRect().width;
            setHeaderPosition(width > wrapBreakpoint ? props.headerPosition! : "top");
        });

        resizeObserver.observe(self);
        return (): void => resizeObserver.unobserve(self);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div
            ref={selfRef}
            className="tabs"
            data-header-position={headerPosition}>

            <div className="tabs-header">
                <TabsContext.Provider value={contextValue}>
                    {props.children}
                </TabsContext.Provider>
            </div>

            <div
                className="tabs-body overflow-x-auto"
                data-selected-tab={selectedTab}>
                {content}
            </div>
        </div>
    );
}