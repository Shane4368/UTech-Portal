export function UTechEmblemComponent(): JSX.Element {
    return (
        <img
            className="utech-emblem"
            alt="UTech Emblem"
            src={process.env.PUBLIC_URL + "/assets/utech-emblem.png"}
        />
    );
}
