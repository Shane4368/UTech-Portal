import React from "react";
import { FaChevronDown } from "react-icons/fa";

import "./ExpanderComponent.scss";

type ExpanderComponentProps = { children: React.ReactNode[]; };

export function ExpanderComponent(props: ExpanderComponentProps): JSX.Element {
    const headerRef = React.useRef<HTMLButtonElement>(null);
    const bodyRef = React.useRef<HTMLDivElement>(null);

    const [isOpen, setOpen] = React.useState(false);
    const [height, setHeight] = React.useState(0);
    const [transition, setTransition] = React.useState("transition-none");

    React.useEffect(() => {
        const body = bodyRef.current!;

        const resizeObserver = new ResizeObserver(() => {
            const header = headerRef.current!;
            const headerHeight = header.getBoundingClientRect().height;

            setHeight(headerHeight + body.offsetHeight);
            setTransition("transition-none");
        });

        resizeObserver.observe(body);
        return (): void => resizeObserver.unobserve(body);
    }, []);

    React.useEffect(() => {
        const header = headerRef.current!;
        const body = bodyRef.current!;

        const headerHeight = header.getBoundingClientRect().height;
        const bodyHeight = body.getBoundingClientRect().height;

        if (isOpen) {
            setHeight(headerHeight + bodyHeight);
        } else {
            setHeight(headerHeight);
        }
    }, [isOpen, height]);

    function handleHeaderClick(e: React.MouseEvent): void {
        setOpen(x => !x);
        setTransition("");
    }

    return (
        <div
            className={`expander ${transition}`}
            style={{ height: height + "px" }}
            data-open={isOpen}>

            <button
                ref={headerRef}
                className="expander-header"
                onClick={handleHeaderClick}>

                <FaChevronDown className="chevron" />
                {props.children[0]}
            </button>

            <div ref={bodyRef} className="expander-body">
                {props.children[1]}
            </div>
        </div>
    );
}