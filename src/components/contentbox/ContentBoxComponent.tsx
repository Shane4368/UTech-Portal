import React from "react";

import "./ContentBoxComponent.scss";

type ContentBoxComponentProps = {
    className?: string;
    caption: string;
    colour?: string;
    children: React.ReactNode;
};

export function ContentBoxComponent(props: ContentBoxComponentProps): JSX.Element {
    const className = props.className ?? "";
    const colour = props.colour ?? "";

    return (
        <table className={`content-box ${className} ${colour}`}>
            <caption>{props.caption}</caption>
            <tbody>{props.children}</tbody>
        </table>
    );
}